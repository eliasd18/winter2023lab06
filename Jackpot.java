public class Jackpot 
{
	public static void main(String[] args) 
	{
		System.out.println("Welcome to Jackpot!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while (gameOver != true) 
		{
			System.out.println(board);
			boolean turn = board.playATurn();
			if (turn == true) 
			{
				gameOver = true;
			}
			else 
			{
				numOfTilesClosed += 1;
			}
		}
		if (numOfTilesClosed >= 7) 
		{
			System.out.println("You reached the jackpot and won!");
		}
		else 
		{
			System.out.println("You lost!");
		}
	}	
}