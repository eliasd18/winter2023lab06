public class Board 
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board() 
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	public boolean playATurn() 
	{
		this.die1.roll();
		System.out.println(this.die1);
		this.die2.roll();
		System.out.println(this.die2);
		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		if (this.tiles[sumOfDice-1] == false) 
		{
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			this.tiles[sumOfDice-1] = true;
			return false;
		}
		else if (this.tiles[this.die1.getFaceValue()-1] == false) 
		{
			System.out.println("Closing tile with the same value as die one: " + this.die1.getFaceValue());
			this.tiles[this.die1.getFaceValue()-1] = true;
			return false;
		}
		else if (this.tiles[this.die2.getFaceValue()-1] == false) 
		{
			System.out.println("Closing tile with the same value as die two: " + this.die2.getFaceValue());
			this.tiles[this.die2.getFaceValue()-1] = true;
			return false;
		}
		else 
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
	public String toString() 
	{
		String s = "";
		for (int i = 0; i < this.tiles.length; i++) 
		{
			if (this.tiles[i] == false) 
			{
				s += (Integer.toString(i+1) + " ");
			}
			else 
			{
				s += "X ";
			}
		}
		return s;
	}
}