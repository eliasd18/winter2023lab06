import java.util.Random;
public class Die 
{
	private int faceValue;
	private Random rollDice;
	
	public Die() 
	{
		this.faceValue = 1;
		this.rollDice = new Random();
	}
	public int getFaceValue() 
	{
		return this.faceValue;
	}
	public void roll() 
	{
		this.faceValue = this.rollDice.nextInt(6);
		this.faceValue += 1;
	}
	public String toString() 
	{
		return "Rolling the die: got "+this.faceValue;
	}
}
